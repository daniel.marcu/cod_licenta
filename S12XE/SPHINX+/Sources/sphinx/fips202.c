/* Based on the public domain implementation in
 * crypto_hash/keccakc512/simple/ from http://bench.cr.yp.to/supercop.html
 * by Ronny Van Keer
 * and the public domain "TweetFips202" implementation
 * from https://twitter.com/tweetfips202
 * by Gilles Van Assche, Daniel J. Bernstein, and Peter Schwabe */
           
#include "stdint.h"
#include <stddef.h>

#include "fips202.h" 
#include "utils.h"

#define NROUNDS 24
#define NROUNDS2 2*NROUNDS
#define ROL(a, offset) xor((shl((a), (offset))) , shr((a), (64 - (offset))))

/*************************************************
 * Name:        load64
 *
 * Description: Load 8 bytes into uint64 in little-endian order
 *
 * Arguments:   - const uint8_t *x: pointer to input byte array
 *
 * Returns the loaded 64-bit uint32_teger
 **************************************************/
static uint64 load64(const uint8_t *x) {
    uint64 r = {0};
    size_t i;
    for (i = 0; i < 8; ++i) {
        r.data[i] = x[i];//TODO sau 8-i
    }

    return r;
}

/*************************************************
 * Name:        store64
 *
 * Description: Store a 64-bit integer to a byte array in little-endian order
 *
 * Arguments:   - uint8_t *x: pointer to the output byte array
 *              - uint64 u: input 64-bit uint32_teger
 **************************************************/
static void store64(uint8_t *x, uint64 u) {
size_t i;
    for (i = 0; i < 8; ++i) {
        x[i] = (uint8_t) (u.data[i]);//TODO sau 8-i
    }
}

/* Keccak round constants */
static const uint32_t KeccakF_RoundConstants1[NROUNDS2] = {
    0x00000000UL, 0x00000001UL, 0x00000000UL, 0x00008082UL,
    0x80000000UL, 0x0000808aUL, 0x80000000UL, 0x80008000UL,
    0x00000000UL, 0x0000808bUL, 0x00000000UL, 0x80000001UL,
    0x80000000UL, 0x80008081UL, 0x80000000UL, 0x00008009UL,
    0x00000000UL, 0x0000008aUL, 0x00000000UL, 0x00000088UL,
    0x00000000UL, 0x80008009UL, 0x00000000UL, 0x8000000aUL,
    0x00000000UL, 0x8000808bUL, 0x80000000UL, 0x0000008bUL,
    0x80000000UL, 0x00008089UL, 0x80000000UL, 0x00008003UL,
    0x80000000UL, 0x00008002UL, 0x80000000UL, 0x00000080UL,
    0x00000000UL, 0x0000800aUL, 0x80000000UL, 0x8000000aUL,
    0x80000000UL, 0x80008081UL, 0x80000000UL, 0x00008080UL,
    0x00000000UL, 0x80000001UL, 0x80000000UL, 0x80008008UL
};
static const uint64 *KeccakF_RoundConstants = KeccakF_RoundConstants1;//TODO
  /*
static const uint64 KeccakF_RoundConstants[NROUNDS] = {
    0x0000000000000001ULL, 0x0000000000008082ULL,
    0x800000000000808aULL, 0x8000000080008000ULL,
    0x000000000000808bULL, 0x0000000080000001ULL,
    0x8000000080008081ULL, 0x8000000000008009ULL,
    0x000000000000008aULL, 0x0000000000000088ULL,
    0x0000000080008009ULL, 0x000000008000000aULL,
    0x000000008000808bULL, 0x800000000000008bULL,
    0x8000000000008089ULL, 0x8000000000008003ULL,
    0x8000000000008002ULL, 0x8000000000000080ULL,
    0x000000000000800aULL, 0x800000008000000aULL,
    0x8000000080008081ULL, 0x8000000000008080ULL,
    0x0000000080000001ULL, 0x8000000080008008ULL
}; */

/*************************************************
 * Name:        KeccakF1600_StatePermute
 *
 * Description: The Keccak F1600 Permutation
 *
 * Arguments:   - uint64 *state: pointer to input/output Keccak state
 **************************************************/
static void KeccakF1600_StatePermute(uint64 *state) {
    int32_t round;

    uint64 Aba, Abe, Abi, Abo, Abu;
    uint64 Aga, Age, Agi, Ago, Agu;
    uint64 Aka, Ake, Aki, Ako, Aku;
    uint64 Ama, Ame, Ami, Amo, Amu;
    uint64 Asa, Ase, Asi, Aso, Asu;
    uint64 BCa, BCe, BCi, BCo, BCu;
    uint64 Da, De, Di, Do, Du;
    uint64 Eba, Ebe, Ebi, Ebo, Ebu;
    uint64 Ega, Ege, Egi, Ego, Egu;
    uint64 Eka, Eke, Eki, Eko, Eku;
    uint64 Ema, Eme, Emi, Emo, Emu;
    uint64 Esa, Ese, Esi, Eso, Esu;

    // copyFromState(A, state)
    Aba = state[0];
    Abe = state[1];
    Abi = state[2];
    Abo = state[3];
    Abu = state[4];
    Aga = state[5];
    Age = state[6];
    Agi = state[7];
    Ago = state[8];
    Agu = state[9];
    Aka = state[10];
    Ake = state[11];
    Aki = state[12];
    Ako = state[13];
    Aku = state[14];
    Ama = state[15];
    Ame = state[16];
    Ami = state[17];
    Amo = state[18];
    Amu = state[19];
    Asa = state[20];
    Ase = state[21];
    Asi = state[22];
    Aso = state[23];
    Asu = state[24];

    for (round = 0; round < NROUNDS; round += 2) {
        //    prepareTheta
        BCa = xor(Aba, xor(Aga, xor(Aka, xor(Ama, Asa))));
        BCe = xor(Abe, xor(Age, xor(Ake, xor(Ame, Ase))));
        BCi = xor(Abi, xor(Agi, xor(Aki, xor(Ami, Asi))));
        BCo = xor(Abo, xor(Ago, xor(Ako, xor(Amo, Aso))));
        BCu = xor(Abu, xor(Agu, xor(Aku, xor(Amu, Asu))));

        // thetaRhoPiChiIotaPrepareTheta(round  , A, E)
        Da = xor(BCu, ROL(BCe, 1));
        De = xor(BCa, ROL(BCi, 1));
        Di = xor(BCe, ROL(BCo, 1));
        Do = xor(BCi, ROL(BCu, 1));
        Du = xor(BCo, ROL(BCa, 1));

        Aba = xor(Aba, Da);
        BCa = Aba;
        Age = xor(Age, De);
        BCe = ROL(Age, 44);
        Aki = xor(Aki, Di);
        BCi = ROL(Aki, 43);
        Amo = xor(Amo, Do);
        BCo = ROL(Amo, 21);
        Asu = xor(Asu, Du);
        BCu = ROL(Asu, 14);
        Eba = xor(BCa, and(neg(BCe), BCi));
        Eba = xor(Eba, KeccakF_RoundConstants[round]);
        Ebe = xor(BCe, and(neg(BCi), BCo));
        Ebi = xor(BCi, and(neg(BCo), BCu));
        Ebo = xor(BCo, and(neg(BCu), BCa));
        Ebu = xor(BCu, and(neg(BCa), BCe));

        Abo = xor(Abo, Do);
        BCa = ROL(Abo, 28);
        Agu = xor(Agu, Du);
        BCe = ROL(Agu, 20);
        Aka = xor(Aka, Da);
        BCi = ROL(Aka, 3);
        Ame = xor(Ame, De);
        BCo = ROL(Ame, 45);
        Asi = xor(Asi, Di);
        BCu = ROL(Asi, 61);
        Ega = xor(BCa, and(neg(BCe), BCi));
        Ege = xor(BCe, and(neg(BCi), BCo));
        Egi = xor(BCi, and(neg(BCo), BCu));
        Ego = xor(BCo, and(neg(BCu), BCa));
        Egu = xor(BCu, and(neg(BCa), BCe));

        Abe = xor(Abe, De);
        BCa = ROL(Abe, 1);
        Agi = xor(Agi, Di);
        BCe = ROL(Agi, 6);
        Ako = xor(Ako, Do);
        BCi = ROL(Ako, 25);
        Amu = xor(Amu, Du);
        BCo = ROL(Amu, 8);
        Asa = xor(Asa, Da);
        BCu = ROL(Asa, 18);
        Eka = xor(BCa, and(neg(BCe), BCi));
        Eke = xor(BCe, and(neg(BCi), BCo));
        Eki = xor(BCi, and(neg(BCo), BCu));
        Eko = xor(BCo, and(neg(BCu), BCa));
        Eku = xor(BCu, and(neg(BCa), BCe));

        Abu = xor(Abu, Du);
        BCa = ROL(Abu, 27);
        Aga = xor(Aga, Da);
        BCe = ROL(Aga, 36);
        Ake = xor(Ake, De);
        BCi = ROL(Ake, 10);
        Ami = xor(Ami, Di);
        BCo = ROL(Ami, 15);
        Aso = xor(Aso, Do);
        BCu = ROL(Aso, 56);
        Ema = xor(BCa, and(neg(BCe), BCi));
        Eme = xor(BCe, and(neg(BCi), BCo));
        Emi = xor(BCi, and(neg(BCo), BCu));
        Emo = xor(BCo, and(neg(BCu), BCa));
        Emu = xor(BCu, and(neg(BCa), BCe));

        Abi = xor(Abi, Di);
        BCa = ROL(Abi, 62);
        Ago = xor(Ago, Do);
        BCe = ROL(Ago, 55);
        Aku = xor(Aku, Du);
        BCi = ROL(Aku, 39);
        Ama = xor(Ama, Da);
        BCo = ROL(Ama, 41);
        Ase = xor(Ase, De);
        BCu = ROL(Ase, 2);
        Esa = xor(BCa, and(neg(BCe), BCi));
        Ese = xor(BCe, and(neg(BCi), BCo));
        Esi = xor(BCi, and(neg(BCo), BCu));
        Eso = xor(BCo, and(neg(BCu), BCa));
        Esu = xor(BCu, and(neg(BCa), BCe));

        //    prepareTheta
        BCa = xor(Eba, xor(Ega, xor(Eka, xor(Ema, Esa))));
        BCe = xor(Ebe, xor(Ege, xor(Eke, xor(Eme, Ese))));
        BCi = xor(Ebi, xor(Egi, xor(Eki, xor(Emi, Esi))));
        BCo = xor(Ebo, xor(Ego, xor(Eko, xor(Emo, Eso))));
        BCu = xor(Ebu, xor(Egu, xor(Eku, xor(Emu, Esu))));

        // thetaRhoPiChiIotaPrepareTheta(round+1, E, A)
        Da = xor(BCu, ROL(BCe, 1));
        De = xor(BCa, ROL(BCi, 1));
        Di = xor(BCe, ROL(BCo, 1));
        Do = xor(BCi, ROL(BCu, 1));
        Du = xor(BCo, ROL(BCa, 1));

        Eba = xor(Eba, Da);
        BCa = Eba;
        Ege = xor(Ege, De);
        BCe = ROL(Ege, 44);
        Eki = xor(Eki, Di);
        BCi = ROL(Eki, 43);
        Emo = xor(Emo, Do);
        BCo = ROL(Emo, 21);
        Esu = xor(Esu, Du);
        BCu = ROL(Esu, 14);
        Aba = xor(BCa, and(neg(BCe), BCi));
        Aba = xor(Aba, KeccakF_RoundConstants[round + 1]);
        Abe = xor(BCe, and(neg(BCi), BCo));
        Abi = xor(BCi, and(neg(BCo), BCu));
        Abo = xor(BCo, and(neg(BCu), BCa));
        Abu = xor(BCu, and(neg(BCa), BCe));

        Ebo = xor(Ebo, Do);
        BCa = ROL(Ebo, 28);
        Egu = xor(Egu, Du);
        BCe = ROL(Egu, 20);
        Eka = xor(Eka, Da);
        BCi = ROL(Eka, 3);
        Eme = xor(Eme, De);
        BCo = ROL(Eme, 45);
        Esi = xor(Esi, Di);
        BCu = ROL(Esi, 61);
        Aga = xor(BCa, and(neg(BCe), BCi));
        Age = xor(BCe, and(neg(BCi), BCo));
        Agi = xor(BCi, and(neg(BCo), BCu));
        Ago = xor(BCo, and(neg(BCu), BCa));
        Agu = xor(BCu, and(neg(BCa), BCe));

        Ebe = xor(Ebe, De);
        BCa = ROL(Ebe, 1);
        Egi = xor(Egi, Di);
        BCe = ROL(Egi, 6);
        Eko = xor(Eko, Do);
        BCi = ROL(Eko, 25);
        Emu = xor(Emu, Du);
        BCo = ROL(Emu, 8);
        Esa = xor(Esa, Da);
        BCu = ROL(Esa, 18);
        Aka = xor(BCa, and(neg(BCe), BCi));
        Ake = xor(BCe, and(neg(BCi), BCo));
        Aki = xor(BCi, and(neg(BCo), BCu));
        Ako = xor(BCo, and(neg(BCu), BCa));
        Aku = xor(BCu, and(neg(BCa), BCe));

        Ebu = xor(Ebu, Du);
        BCa = ROL(Ebu, 27);
        Ega = xor(Ega, Da);
        BCe = ROL(Ega, 36);
        Eke = xor(Eke, De);
        BCi = ROL(Eke, 10);
        Emi = xor(Emi, Di);
        BCo = ROL(Emi, 15);
        Eso = xor(Eso, Do);
        BCu = ROL(Eso, 56);
        Ama = xor(BCa, and(neg(BCe), BCi));
        Ame = xor(BCe, and(neg(BCi), BCo));
        Ami = xor(BCi, and(neg(BCo), BCu));
        Amo = xor(BCo, and(neg(BCu), BCa));
        Amu = xor(BCu, and(neg(BCa), BCe));

        Ebi = xor(Ebi, Di);
        BCa = ROL(Ebi, 62);
        Ego = xor(Ego, Do);
        BCe = ROL(Ego, 55);
        Eku = xor(Eku, Du);
        BCi = ROL(Eku, 39);
        Ema = xor(Ema, Da);
        BCo = ROL(Ema, 41);
        Ese = xor(Ese, De);
        BCu = ROL(Ese, 2);
        Asa = xor(BCa, and(neg(BCe), BCi));
        Ase = xor(BCe, and(neg(BCi), BCo));
        Asi = xor(BCi, and(neg(BCo), BCu));
        Aso = xor(BCo, and(neg(BCu), BCa));
        Asu = xor(BCu, and(neg(BCa), BCe));
    }

    // copyToState(state, A)
    state[0] = Aba;
    state[1] = Abe;
    state[2] = Abi;
    state[3] = Abo;
    state[4] = Abu;
    state[5] = Aga;
    state[6] = Age;
    state[7] = Agi;
    state[8] = Ago;
    state[9] = Agu;
    state[10] = Aka;
    state[11] = Ake;
    state[12] = Aki;
    state[13] = Ako;
    state[14] = Aku;
    state[15] = Ama;
    state[16] = Ame;
    state[17] = Ami;
    state[18] = Amo;
    state[19] = Amu;
    state[20] = Asa;
    state[21] = Ase;
    state[22] = Asi;
    state[23] = Aso;
    state[24] = Asu;
}

/*************************************************
 * Name:        keccak_absorb
 *
 * Description: Absorb step of Keccak;
 *              non-incremental, starts by zeroeing the state.
 *
 * Arguments:   - uint64 *s: pointer to (uninitialized) output Keccak state
 *              - uint32_t r: rate in bytes (e.g., 168 for SHAKE128)
 *              - const uint8_t *m: pointer to input to be absorbed into s
 *              - size_t mlen: length of input in bytes
 *              - uint8_t p: domain-separation byte for different
 *                                 Keccak-derived functions
 **************************************************/
static void keccak_absorb(uint64 *s, uint32_t r, const uint8_t *m,
                          size_t mlen, uint8_t p) {
    size_t i, j;
    uint8_t t[200];

    /* Zero state */
    for (i = 0; i < 25; ++i) {
        for(j=0;j<8;j++)
        s[i].data[j] = 0;
    }

    while (mlen >= r) {
        for (i = 0; i < r / 8; ++i) {
            s[i] = xor(s[i], load64(m + 8 * i));
        }

        KeccakF1600_StatePermute(s);
        mlen -= r;
        m += r;
    }

    for (i = 0; i < r; ++i) {
        t[i] = 0;
    }
    for (i = 0; i < mlen; ++i) {
        t[i] = m[i];
    }
    t[i] = p;
    t[r - 1] |= 128;
    for (i = 0; i < r / 8; ++i) {
        s[i] = xor(s[i], load64(t + 8 * i));
    }
}

/*************************************************
 * Name:        keccak_squeezeblocks
 *
 * Description: Squeeze step of Keccak. Squeezes full blocks of r bytes each.
 *              Modifies the state. Can be called multiple times to keep
 *              squeezing, i.e., is incremental.
 *
 * Arguments:   - uint8_t *h: pointer to output blocks
 *              - size_t nblocks: number of blocks to be
 *                                                squeezed (written to h)
 *              - uint64 *s: pointer to input/output Keccak state
 *              - uint32_t r: rate in bytes (e.g., 168 for SHAKE128)
 **************************************************/
static void keccak_squeezeblocks(uint8_t *h, size_t nblocks,
                                 uint64 *s, uint32_t r) {
    size_t i;
    while (nblocks > 0) {
        KeccakF1600_StatePermute(s);
        for (i = 0; i < (r >> 3); i++) {
            store64(h + 8 * i, s[i]);
        }
        h += r;
        nblocks--;
    }
}

/*************************************************
 * Name:        keccak_inc_init
 *
 * Description: Initializes the incremental Keccak state to zero.
 *
 * Arguments:   - uint64 *s_inc: pointer to input/output incremental state
 *                First 25 values represent Keccak state.
 *                26th value represents either the number of absorbed bytes
 *                that have not been permuted, or not-yet-squeezed bytes.
 **************************************************/
static void keccak_inc_init(uint64 *s_inc) {
    size_t i, j;

    for (i = 0; i <= 25; ++i) {
        for(j=0;j<8;j++)
            s_inc[i].data[j] = 0;
    }
}

/*************************************************
 * Name:        keccak_inc_absorb
 *
 * Description: Incremental keccak absorb
 *              Preceded by keccak_inc_init, succeeded by keccak_inc_finalize
 *
 * Arguments:   - uint64 *s_inc: pointer to input/output incremental state
 *                First 25 values represent Keccak state.
 *                26th value represents either the number of absorbed bytes
 *                that have not been permuted, or not-yet-squeezed bytes.
 *              - uint32_t r: rate in bytes (e.g., 168 for SHAKE128)
 *              - const uint8_t *m: pointer to input to be absorbed into s
 *              - size_t mlen: length of input in bytes
 **************************************************/
static void keccak_inc_absorb(uint64 *s_inc, uint32_t r, const uint8_t *m,
                              size_t mlen) {
    size_t i,j;
    uint64 tmp={0};
    /* Recall that s_inc[25] is the non-absorbed bytes xored into the state */
    while (mlen + ull_to_uint32(s_inc[25]) >= r) {
        for (i = 0; i < r - ull_to_uint32(s_inc[25]); i++) {
            /* Take the i'th byte from message
               xor with the s_inc[25] + i'th byte of the state; little-endian */
               
            for(j=0;j<8;j++)
                tmp.data[i]=0;
            tmp.data[7]=m[i];
            s_inc[ull_to_uint32(shr(add(s_inc[25], i), 3))] = xor( 
                s_inc[ull_to_uint32(shr(add(s_inc[25], i), 3))],
                shl(tmp, (8 * (ull_to_uint32(add(s_inc[25],  i)) & 0x07)))
            );
        }
        mlen -= (size_t)(r - ull_to_uint32(s_inc[25]));
        m += r - ull_to_uint32(s_inc[25]);
        for(j=0;j<8;j++)
            s_inc[25].data[j] = 0;

        KeccakF1600_StatePermute(s_inc);
    }

    for (i = 0; i < mlen; i++) {
        for(j=0;j<8;j++)
            tmp.data[i]=0;
        tmp.data[7]=m[i];
        s_inc[ull_to_uint32(shr(add(s_inc[25], i), 3))] = xor(
            s_inc[ull_to_uint32(shr(add(s_inc[25], i), 3))],
            shl(tmp, (8 * (ull_to_uint32(add(s_inc[25], i)) & 0x07)))
        );
    }
    s_inc[25] = add(s_inc[25], mlen);
}

/*************************************************
 * Name:        keccak_inc_finalize
 *
 * Description: Finalizes Keccak absorb phase, prepares for squeezing
 *
 * Arguments:   - uint64 *s_inc: pointer to input/output incremental state
 *                First 25 values represent Keccak state.
 *                26th value represents either the number of absorbed bytes
 *                that have not been permuted, or not-yet-squeezed bytes.
 *              - uint32_t r: rate in bytes (e.g., 168 for SHAKE128)
 *              - uint8_t p: domain-separation byte for different
 *                                 Keccak-derived functions
 **************************************************/
static void keccak_inc_finalize(uint64 *s_inc, uint32_t r, uint8_t p) {
    /* After keccak_inc_absorb, we are guaranteed that s_inc[25] < r,
       so we can always use one more byte for p in the current state. */
    uint64 tmp={0};
    uint32_t i;
    tmp.data[7]=p;
    s_inc[ull_to_uint32(shr(s_inc[25], 3))] = xor(
        s_inc[ull_to_uint32(shr(s_inc[25], 3))],
        shl(tmp , (8 * (ull_to_uint32(s_inc[25]) & 0x07)))
    );
    for(i=0;i<8;i++)
        tmp.data[i]=0;
    tmp.data[7]=128;
    s_inc[(r - 1) >> 3] = xor(s_inc[(r - 1) >> 3], shl(tmp, (8 * ((r - 1) & 0x07))));
    s_inc[25] = uint32_to_ull(0);
}

/*************************************************
 * Name:        keccak_inc_squeeze
 *
 * Description: Incremental Keccak squeeze; can be called on byte-level
 *
 * Arguments:   - uint8_t *h: pointer to output bytes
 *              - size_t outlen: number of bytes to be squeezed
 *              - uint64 *s_inc: pointer to input/output incremental state
 *                First 25 values represent Keccak state.
 *                26th value represents either the number of absorbed bytes
 *                that have not been permuted, or not-yet-squeezed bytes.
 *              - uint32_t r: rate in bytes (e.g., 168 for SHAKE128)
 **************************************************/
static void keccak_inc_squeeze(uint8_t *h, size_t outlen,
                               uint64 *s_inc, uint32_t r) {
    size_t i;

    /* First consume any bytes we still have sitting around */
    for (i = 0; i < outlen && i < ull_to_uint32(s_inc[25]); i++) {
        /* There are s_inc[25] bytes left, so r - s_inc[25] is the first
           available byte. We consume from there, i.e., up to r. */
        h[i] = (uint8_t)ull_to_uint32(
            shr(s_inc[(r - ull_to_uint32(s_inc[25]) + i) >> 3], (8 * ((r - ull_to_uint32(s_inc[25]) + i) & 0x07)))
        );
    }
    h += i;
    outlen -= i;
    s_inc[25] = sub(s_inc[25], i);

    /* Then squeeze the remaining necessary blocks */
    while (outlen > 0) {
        KeccakF1600_StatePermute(s_inc);

        for (i = 0; i < outlen && i < r; i++) {
            h[i] = (uint8_t)ull_to_uint32(shr(s_inc[i >> 3], (8 * (i & 0x07))));
        }
        h += i;
        outlen -= i;
        s_inc[25] = uint32_to_ull( r - i);
    }
}

void shake256_inc_init(uint64 *s_inc) {
    keccak_inc_init(s_inc);
}

void shake256_inc_absorb(uint64 *s_inc, const uint8_t *input, size_t inlen) {
    keccak_inc_absorb(s_inc, SHAKE256_RATE, input, inlen);
}

void shake256_inc_finalize(uint64 *s_inc) {
    keccak_inc_finalize(s_inc, SHAKE256_RATE, 0x1F);
}

void shake256_inc_squeeze(uint8_t *output, size_t outlen, uint64 *s_inc) {
    keccak_inc_squeeze(output, outlen, s_inc, SHAKE256_RATE);
}

/*************************************************
 * Name:        shake256_absorb
 *
 * Description: Absorb step of the SHAKE256 XOF.
 *              non-incremental, starts by zeroeing the state.
 *
 * Arguments:   - uint64 *s: pointer to (uninitialized) output Keccak state
 *              - const uint8_t *input: pointer to input to be absorbed
 *                                            into s
 *              - size_t inlen: length of input in bytes
 **************************************************/
void shake256_absorb(uint64 *s, const uint8_t *input, size_t inlen) {
    keccak_absorb(s, SHAKE256_RATE, input, inlen, 0x1F);
}

/*************************************************
 * Name:        shake256_squeezeblocks
 *
 * Description: Squeeze step of SHAKE256 XOF. Squeezes full blocks of
 *              SHAKE256_RATE bytes each. Modifies the state. Can be called
 *              multiple times to keep squeezing, i.e., is incremental.
 *
 * Arguments:   - uint8_t *output: pointer to output blocks
 *              - size_t nblocks: number of blocks to be squeezed
 *                                (written to output)
 *              - uint64 *s: pointer to input/output Keccak state
 **************************************************/
void shake256_squeezeblocks(uint8_t *output, size_t nblocks, uint64 *s) {
    keccak_squeezeblocks(output, nblocks, s, SHAKE256_RATE);
}

/*************************************************
 * Name:        shake256
 *
 * Description: SHAKE256 XOF with non-incremental API
 *
 * Arguments:   - uint8_t *output: pointer to output
 *              - size_t outlen: requested output length in bytes
 *              - const uint8_t *input: pointer to input
 *              - size_t inlen: length of input in bytes
 **************************************************/
void shake256(uint8_t *output, size_t outlen,
              const uint8_t *input, size_t inlen) {
    size_t nblocks = outlen / SHAKE256_RATE;
    uint8_t t[SHAKE256_RATE];
    uint64 s[25];
    size_t i;

    shake256_absorb(s, input, inlen);
    shake256_squeezeblocks(output, nblocks, s);

    output += nblocks * SHAKE256_RATE;
    outlen -= nblocks * SHAKE256_RATE;

    if (outlen) {
        shake256_squeezeblocks(t, 1, s);
        for (i = 0; i < outlen; ++i) {
            output[i] = t[i];
        }
    }
}
