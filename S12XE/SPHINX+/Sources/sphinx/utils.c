#include <string.h>

#include "utils.h"
#include "params.h"
#include "hash.h"
#include "thash.h"
#include "address.h"
#include "stdint.h"

/**
 * Converts the value of 'in' to 'outlen' bytes in big-endian byte order.
 */
void ull_to_bytes(unsigned char *out, uint32_t outlen,
                  uint64 in)
{
    int32_t i;

    /* Iterate over out in decreasing order, for big-endianness. */
    for (i = (signed int)outlen - 1; i >= 0; i--) {
        out[i] = in.data[i];
    }
}
uint64 uint32_to_ull(uint32_t in){
    uint64 a;
    a.data[0] = (unsigned char)(in >> 24);
    a.data[1] = (unsigned char)(in >> 16);
    a.data[2] = (unsigned char)(in >> 8);
    a.data[3] = (unsigned char)in;
    return a;
}
uint64 xor(uint64 a, uint64 b){
    uint64 c;
    c.data[0]=a.data[0]^b.data[0];
    c.data[1]=a.data[1]^b.data[1];
    c.data[2]=a.data[2]^b.data[2];
    c.data[3]=a.data[3]^b.data[3];
    c.data[4]=a.data[4]^b.data[4];
    c.data[5]=a.data[5]^b.data[5];
    c.data[6]=a.data[6]^b.data[6];
    c.data[7]=a.data[7]^b.data[7];
    return c;
}
uint64 and(uint64 a, uint64 b){
    uint64 c;
    c.data[0]=a.data[0]&b.data[0];
    c.data[1]=a.data[1]&b.data[1];
    c.data[2]=a.data[2]&b.data[2];
    c.data[3]=a.data[3]&b.data[3];
    c.data[4]=a.data[4]&b.data[4];
    c.data[5]=a.data[5]&b.data[5];
    c.data[6]=a.data[6]&b.data[6];
    c.data[7]=a.data[7]&b.data[7];
    return c;
}
uint64 or(uint64 a, uint64 b){
    uint64 c;
    c.data[0]=a.data[0]|b.data[0];
    c.data[1]=a.data[1]|b.data[1];
    c.data[2]=a.data[2]|b.data[2];
    c.data[3]=a.data[3]|b.data[3];
    c.data[4]=a.data[4]|b.data[4];
    c.data[5]=a.data[5]|b.data[5];
    c.data[6]=a.data[6]|b.data[6];
    c.data[7]=a.data[7]|b.data[7];
    return c;
}
uint64 neg(uint64 a){
    uint64 c;
    c.data[0] = ~a.data[0];
    c.data[1] = ~a.data[1];
    c.data[2] = ~a.data[2];
    c.data[3] = ~a.data[3];
    c.data[4] = ~a.data[4];
    c.data[5] = ~a.data[5];
    c.data[6] = ~a.data[6];
    c.data[7] = ~a.data[7];
    return c;
}
uint64 add(uint64 a, uint32_t b){
    uint8_t c1[8]={0}, c2[8]={0};
    uint8_t res[8] = {0};
    uint8_t carry=0;
    int8_t i=0;
    ull_to_bytes(c1, 8, a);
    u32_to_bytes(c2+4, b);
    for(i=7;i>=0;i--){
        res[i]=c1[i]+c2[i]+carry;
        if(res[i]<c1[i] || res[i]<c2[i])
            carry=1;
        else
            carry=0;
    }
    return bytes_to_ull(res,8);
}
uint64 sub(uint64 a, uint32_t b){
    uint8_t c1[8]={0}, c2[8]={0};
    uint8_t res[8] = {0};
    uint8_t carry=0;
    int8_t i=0;
    ull_to_bytes(c1, 8, a);
    u32_to_bytes(c2+4, b);
    for(i=7;i>=0;i--){
        res[i]=c1[i]-c2[i]-carry;
        if(res[i]>c1[i])
            carry=1;
        else
            carry=0;
    }
    return bytes_to_ull(res,8);
}

uint64 shr2(uint64 num, uint32_t n){
    uint32_t n1=n/8;
    uint8_t n2=n%8;
    uint8_t carry1=0, carry2=0;
    uint8_t mask=0;
    uint32_t i;
    uint64 out=num;
    for(i=7;i>=n1;i--)
        out.data[i]=out.data[i-n1];
    for(i=n1-1;i>0;i--)
        out.data[i]=0;
    for(i=n1; i<8;i++){
        carry2=(out.data[i]<<(8-n2));
        out.data[i] = (out.data[i]>>n2)|carry1;
        carry1=carry2;
    }
    return out;
}
uint64 shl2(uint64 num, uint32_t n){//aprox 200 instr
    uint32_t n1=n/8;
    uint8_t n2=n%8;
    uint8_t carry1=0, carry2=0;
    uint8_t mask=0;
    uint32_t i;
    uint64 out=num;
    for(i=0;i<8-n1;i++)
        out.data[i]=out.data[i+n1];
    for(i=8-n1;i<8;i++)
        out.data[i]=0;
    for(i=n1-1; i>=0;i--){
        carry2=(out.data[i]>>(8-n2));
        out.data[i] = (out.data[i]<<n2)|carry1;
        carry1=carry2;
    }
    return out;
}
uint64 shl(uint64 num, uint32_t n){
    uint32_t a=0, b=0;
    uint64 out;
    uint32_t carry=0;

    
    
    if(n>=32){
        a |= num.data[4];
        a <<= 8;
        a |= num.data[5];
        a <<= 8;
        a |= num.data[6];
        a <<= 8;
        a |= num.data[7];
        b=0;

        a <<= n-32;

    }else{
        a |= num.data[0];
        a <<= 8;
        a |= num.data[1];
        a <<= 8;
        a |= num.data[2];
        a <<= 8;
        a |= num.data[3];

        b |= num.data[4];
        b <<= 8;
        b |= num.data[5];
        b <<= 8;
        b |= num.data[6];
        b <<= 8;
        b |= num.data[7];

        carry = b>>(32-n);
        b<<=n;
        a<<=n;
        a=a|carry;

    }

    out.data[0] = (unsigned char)(a >> 24);
    out.data[1] = (unsigned char)(a >> 16);
    out.data[2] = (unsigned char)(a >> 8);
    out.data[3] = (unsigned char)a;
    out.data[4] = (unsigned char)(b >> 24);
    out.data[5] = (unsigned char)(b >> 16);
    out.data[6] = (unsigned char)(b >> 8);
    out.data[7] = (unsigned char)b;
    return out;
}


uint64 shr(uint64 num, uint32_t n){
    uint32_t a=0, b=0;
    uint64 out;
    uint32_t carry=0;

    
    
    if(n>=32){
        b |= num.data[4];
        b <<= 8;
        b |= num.data[5];
        b <<= 8;
        b |= num.data[6];
        b <<= 8;
        b |= num.data[7];
        a=0;

        b <<= n-32;

    }else{
        a |= num.data[0];
        a <<= 8;
        a |= num.data[1];
        a <<= 8;
        a |= num.data[2];
        a <<= 8;
        a |= num.data[3];

        b |= num.data[4];
        b <<= 8;
        b |= num.data[5];
        b <<= 8;
        b |= num.data[6];
        b <<= 8;
        b |= num.data[7];

        carry = a<<(32-n);
        b>>=n;
        a>>=n;
        b=b|carry;

    }

    out.data[0] = (unsigned char)(a >> 24);
    out.data[1] = (unsigned char)(a >> 16);
    out.data[2] = (unsigned char)(a >> 8);
    out.data[3] = (unsigned char)a;
    out.data[4] = (unsigned char)(b >> 24);
    out.data[5] = (unsigned char)(b >> 16);
    out.data[6] = (unsigned char)(b >> 8);
    out.data[7] = (unsigned char)b;
    return out;
}

uint64 shr_one(uint64 num){
    int32_t i;
    for (i = 7;  i > 0;  --i)
        num.data[i] = (num.data[i] >> 1) | ((num.data[i-1] << 7) & 0x7F);
    num.data[0] = num.data[0] >> 1;
    return num;
}
uint64 shl_one(uint64 num){
    int32_t i;
    for (i = 0;  i < 7;  ++i)
        num.data[i] = (num.data[i] << 1) | ((num.data[i+1] >> 7) & 1);
    num.data[7] = num.data[7] << 1;
    return num;
}
uint64 shl_old(uint64 num, uint32_t n){
    uint32_t i;
    uint64 out=num;
    for(i=0;i<n;i++)
        num=shl_one(num);
    return out;
}
uint64 shr_old(uint64 num, uint32_t n){
    uint32_t i;
    uint64 out=num;
    for(i=0;i<n;i++)
        num=shr_one(num);
    return out;
}


uint32_t ull_to_uint32(uint64 in){
    uint32_t a=0;
    a |= in.data[0];
    a <<= 8;
    a |= in.data[1];
    a <<= 8;
    a |= in.data[2];
    a <<= 8;
    a |= in.data[3];
    return a;
}

void u32_to_bytes(unsigned char *out, uint32_t in)
{
    out[0] = (unsigned char)(in >> 24);
    out[1] = (unsigned char)(in >> 16);
    out[2] = (unsigned char)(in >> 8);
    out[3] = (unsigned char)in;
}
uint32_t bytes_to_u32(unsigned char *in)
{
    uint32_t out=0;
    out |= in[0];
    out <<= 8;
    out |= in[1];
    out <<= 8;
    out |= in[2];
    out <<= 8;
    out |= in[3];
    return out;
}

/**
 * Converts the inlen bytes in 'in' from big-endian byte order to an integer.
 */
uint64 bytes_to_ull(const unsigned char *in, uint32_t inlen)
{
    uint64 retval = {0};
    uint32_t i;

    for (i = 0; i < inlen; i++) {
        retval.data[i] = in[i];
    }
    return retval;
}

/**
 * Computes a root node given a leaf and an auth path.
 * Expects address to be complete other than the tree_height and tree_index.
 */
void compute_root(unsigned char *root, const unsigned char *leaf,
                  uint32_t leaf_idx, uint32_t idx_offset,
                  const unsigned char *auth_path, uint32_t tree_height,
                  const spx_ctx *ctx, uint32_t addr[8])
{
    uint32_t i;
    unsigned char buffer[2 * SPX_N];

    /* If leaf_idx is odd (last bit = 1), current path element is a right child
       and auth_path has to go left. Otherwise it is the other way around. */
    if (leaf_idx & 1) {
        memcpy(buffer + SPX_N, leaf, SPX_N);
        memcpy(buffer, auth_path, SPX_N);
    }
    else {
        memcpy(buffer, leaf, SPX_N);
        memcpy(buffer + SPX_N, auth_path, SPX_N);
    }
    auth_path += SPX_N;

    for (i = 0; i < tree_height - 1; i++) {
        leaf_idx >>= 1;
        idx_offset >>= 1;
        /* Set the address of the node we're creating. */
        set_tree_height(addr, i + 1);
        set_tree_index(addr, leaf_idx + idx_offset);

        /* Pick the right or left neighbor, depending on parity of the node. */
        if (leaf_idx & 1) {
            thash(buffer + SPX_N, buffer, 2, ctx, addr);
            memcpy(buffer, auth_path, SPX_N);
        }
        else {
            thash(buffer, buffer, 2, ctx, addr);
            memcpy(buffer + SPX_N, auth_path, SPX_N);
        }
        auth_path += SPX_N;
    }

    /* The last iteration is exceptional; we do not copy an auth_path node. */
    leaf_idx >>= 1;
    idx_offset >>= 1;
    set_tree_height(addr, tree_height);
    set_tree_index(addr, leaf_idx + idx_offset);
    thash(root, buffer, 2, ctx, addr);
}

/**
 * For a given leaf index, computes the authentication path and the resulting
 * root node using Merkle's TreeHash algorithm.
 * Expects the layer and tree parts of the tree_addr to be set, as well as the
 * tree type (i.e. SPX_ADDR_TYPE_HASHTREE or SPX_ADDR_TYPE_FORSTREE).
 * Applies the offset idx_offset to indices before building addresses, so that
 * it is possible to continue counting indices across trees.
 */
uint8_t stack[(SPX_FULL_HEIGHT+1)*SPX_N];
uint32_t heights[SPX_FULL_HEIGHT+1];

void treehash(unsigned char *root, unsigned char *auth_path, const spx_ctx* ctx,
              uint32_t leaf_idx, uint32_t idx_offset, uint32_t tree_height,
              void (*gen_leaf)(
                 unsigned char* /* leaf */,
                 const spx_ctx* /* ctx */,
                 uint32_t /* addr_idx */, const uint32_t[8] /* tree_addr */),
              uint32_t tree_addr[8])
{
    //SPX_VLA(uint8_t, stack, (SPX_FULL_HEIGHT+1)*SPX_N);//(tree_height+1)*SPX_N //TODO
    //SPX_VLA(uint32_t, heights, SPX_FULL_HEIGHT+1);//tree_height+1 //TODO
    uint32_t offset = 0;
    uint32_t idx;
    uint32_t tree_idx;

    for (idx = 0; idx < (uint32_t)(1 << tree_height); idx++) {
        /* Add the next leaf node to the stack. */
        gen_leaf(stack + offset*SPX_N, ctx, idx + idx_offset, tree_addr);
        offset++;
        heights[offset - 1] = 0;

        /* If this is a node we need for the auth path.. */
        if ((leaf_idx ^ 0x1) == idx) {
            memcpy(auth_path, stack + (offset - 1)*SPX_N, SPX_N);
        }

        /* While the top-most nodes are of equal height.. */
        while (offset >= 2 && heights[offset - 1] == heights[offset - 2]) {
            /* Compute index of the new node, in the next layer. */
            tree_idx = (idx >> (heights[offset - 1] + 1));

            /* Set the address of the node we're creating. */
            set_tree_height(tree_addr, heights[offset - 1] + 1);
            set_tree_index(tree_addr,
                           tree_idx + (idx_offset >> (heights[offset-1] + 1)));
            /* Hash the top-most nodes from the stack together. */
            thash(stack + (offset - 2)*SPX_N,
                  stack + (offset - 2)*SPX_N, 2, ctx, tree_addr);
            offset--;
            /* Note that the top-most node is now one layer higher. */
            heights[offset - 1]++;

            /* If this is a node we need for the auth path.. */
            if (((leaf_idx >> heights[offset - 1]) ^ 0x1) == tree_idx) {
                memcpy(auth_path + heights[offset - 1]*SPX_N,
                       stack + (offset - 1)*SPX_N, SPX_N);
            }
        }
    }
    memcpy(root, stack, SPX_N);
}
