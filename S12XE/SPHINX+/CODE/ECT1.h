/** ###################################################################
**     THIS COMPONENT MODULE IS GENERATED BY THE TOOL. DO NOT MODIFY IT.
**     Filename  : ECT1.h
**     Project   : test_aes
**     Processor : MC9S12XEP100MAL
**     Component : Init_ECT
**     Version   : Component 01.220, Driver 01.09, CPU db: 3.00.035
**     Compiler  : CodeWarrior HCS12X C Compiler
**     Date/Time : 05.03.2024, 03:10
**     Abstract  :
**          This file implements the ECT (ECT_Counter) module initialization
**          according to the Peripheral Initialization Bean settings,
**          and defines interrupt service routines prototypes.
**          The HCS12 Enhanced Capture Timer module has the features of the
**          HCS12 Standard Timer module enhanced by additional features
**          in order to enlarge the field of applications, in particular
**          for automotive ABS applications.
**          Features
**          � 16-Bit Buffer Register for four Input Capture (IC) channels.
**          � Four 8-Bit Pulse Accumulators with 8-bit buffer registers
**          associated with the four buffered IC channels. Configurable
**          also as two 16-Bit Pulse Accumulators.
**          � 16-Bit Modulus Down-Counter with 4-bit Prescaler.
**          � Four user selectable Delay Counters for input noise immunity increase.
**     Settings  :
**          Component name                                 : ECT1
**          Device                                         : ECT
**          Settings                                       : 
**            Clock settings                               : 
**              Prescaler                                  : Bus Clock / 128
**              Timer Clock Source                         : Timer prescaler
**              Frequency                                  : 62.5 kHz
**              Precision timer                            : Disabled
**            Fast Flag Clear                              : no
**            Timer Counter Reset                          : Disabled
**            Stop in wait mode                            : no
**            Stop in freeze mode                          : no
**            Delay Counter                                : Disabled
**            Mode                                         : Queue
**            Buffer                                       : no
**            Timer flag mode                              : New value in Capture
**            8-bit Pulse accumulator max. count           : no
**            Share input action                           : 
**              Share capture channels 3 & 7               : no
**              Share capture channels 2 & 6               : no
**              Share capture channels 1 & 5               : no
**              Share capture channels 0 & 4               : no
**            Accumulator A                                : 
**              Accumulator counter                        : ECTPACNT23
**              Pulse Accumulator Mode                     : Event counter
**                Edge Control                             : Falling
**              Count register 2                           : 0
**              Count register 3                           : 0
**            Accumulator B                                : 
**              Accumulator counter                        : ECTPACNT01
**              Count register 0                           : 0
**              Count register 1                           : 0
**            Modulus Counter                              : 
**              Modulus counter                            : ModulusCounter
**              Modulus mode                               : no
**              Read                                       : Value of the count register
**              Count register                             : 65535
**              Clock settings                             : 
**                Prescaler                                : 1
**                Frequency                                : 8000 kHz
**          Channels                                       : 
**            Channel 0                                    : 
**              Channel device                             : ECTTC0
**              Mode                                       : Capture
**              Compare Mask                               : Disabled
**              Compare Data                               : 0
**              Toggle On Overflow                         : no
**              Output action                              : Disconected
**              Output Compare Pin                         : Enabled
**              Input control                              : Capture disabled
**              Capture/Compare register                   : 0
**              Capture register Overwrite                 : no
**              Channel 0 interrupt                        : 
**                Channel interrupt                        : Disabled
**                Interrupt                                : Vectch0
**                Priority                                 : 1
**                ISR name                                 : isrVectch0
**              Pin                                        : Disabled
**            Channel 1                                    : 
**              Channel device                             : ECTTC1
**              Mode                                       : Capture
**              Compare Mask                               : Disabled
**              Compare Data                               : 0
**              Toggle On Overflow                         : no
**              Output action                              : Disconected
**              Output Compare Pin                         : Enabled
**              Input control                              : Capture disabled
**              Capture/Compare register                   : 0
**              Capture register Overwrite                 : no
**              Channel 1 interrupt                        : 
**                Channel interrupt                        : Disabled
**                Interrupt                                : Vectch1
**                Priority                                 : 1
**                ISR name                                 : isrVectch1
**              Pin                                        : Disabled
**            Channel 2                                    : 
**              Channel device                             : ECTTC2
**              Mode                                       : Capture
**              Compare Mask                               : Disabled
**              Compare Data                               : 0
**              Toggle On Overflow                         : no
**              Output action                              : Disconected
**              Output Compare Pin                         : Enabled
**              Input control                              : Capture disabled
**              Capture/Compare register                   : 0
**              Capture register Overwrite                 : no
**              Channel 2 interrupt                        : 
**                Channel interrupt                        : Disabled
**                Interrupt                                : Vectch2
**                Priority                                 : 1
**                ISR name                                 : isrVectch2
**              Pin                                        : Disabled
**            Channel 3                                    : 
**              Channel device                             : ECTTC3
**              Mode                                       : Capture
**              Compare Mask                               : Disabled
**              Compare Data                               : 0
**              Toggle On Overflow                         : no
**              Output action                              : Disconected
**              Output Compare Pin                         : Enabled
**              Input control                              : Capture disabled
**              Capture/Compare register                   : 0
**              Capture register Overwrite                 : no
**              Channel 3 interrupt                        : 
**                Channel interrupt                        : Disabled
**                Interrupt                                : Vectch3
**                Priority                                 : 1
**                ISR name                                 : isrVectch3
**              Pin                                        : Disabled
**            Channel 4                                    : 
**              Channel device                             : ECTTC4
**              Mode                                       : Capture
**              Compare Mask                               : Disabled
**              Compare Data                               : 0
**              Toggle On Overflow                         : no
**              Output action                              : Disconected
**              Output Compare Pin                         : Enabled
**              Input control                              : Capture disabled
**              Capture/Compare register                   : 0
**              Capture register Overwrite                 : no
**              Channel 4 interrupt                        : 
**                Channel interrupt                        : Disabled
**                Interrupt                                : Vectch4
**                Priority                                 : 1
**                ISR name                                 : isrVectch4
**              Pin                                        : Disabled
**            Channel 5                                    : 
**              Channel device                             : ECTTC5
**              Mode                                       : Capture
**              Compare Mask                               : Disabled
**              Compare Data                               : 0
**              Toggle On Overflow                         : no
**              Output action                              : Disconected
**              Output Compare Pin                         : Enabled
**              Input control                              : Capture disabled
**              Capture/Compare register                   : 0
**              Capture register Overwrite                 : no
**              Channel 5 interrupt                        : 
**                Channel interrupt                        : Disabled
**                Interrupt                                : Vectch5
**                Priority                                 : 1
**                ISR name                                 : isrVectch5
**              Pin                                        : Disabled
**            Channel 6                                    : 
**              Channel device                             : ECTTC6
**              Mode                                       : Capture
**              Compare Mask                               : Disabled
**              Compare Data                               : 0
**              Toggle On Overflow                         : no
**              Output action                              : Disconected
**              Output Compare Pin                         : Enabled
**              Input control                              : Capture disabled
**              Capture/Compare register                   : 0
**              Capture register Overwrite                 : no
**              Channel 6 interrupt                        : 
**                Channel interrupt                        : Disabled
**                Interrupt                                : Vectch6
**                Priority                                 : 1
**                ISR name                                 : isrVectch6
**              Pin                                        : Disabled
**            Channel 7                                    : 
**              Channel device                             : ECTTC7
**              Mode                                       : Capture
**              Compare Mask                               : Disabled
**              Compare Data                               : 0
**              Toggle On Overflow                         : no
**              Output action                              : Disconected
**              Output Compare Pin                         : Enabled
**              Input control                              : Capture disabled
**              Capture/Compare register                   : 0
**              Capture register Overwrite                 : no
**              Channel 7 interrupt                        : 
**                Channel interrupt                        : Disabled
**                Interrupt                                : Vectch7
**                Priority                                 : 1
**                ISR name                                 : isrVectch7
**              Pin                                        : Disabled
**          Interrupts                                     : 
**            Timer Overflow                               : 
**              Timer Overflow                             : Disabled
**              Interrupt                                  : Vectovf
**              Priority                                   : 1
**              ISR name                                   : timerInterrupt
**            Accumulator A Overflow                       : 
**              Accumulator A Overflow                     : Disabled
**              Interrupt                                  : Vectpaaovf
**              Priority                                   : 1
**              ISR name                                   : isrVectpaaovf
**            Accumulator A Input                          : 
**              Accumulator A Input                        : Disabled
**              Interrupt                                  : Vectpaie
**              Priority                                   : 1
**              ISR name                                   : isrVectpaie
**            Accumulator B Overflow                       : 
**              Accumulator B Overflow                     : Disabled
**              Interrupt                                  : Vectpabovf
**              Priority                                   : 1
**              ISR name                                   : isrVectpabovf
**            Modulus counter Underflow                    : 
**              Modulus counter Underflow                  : Disabled
**              Interrupt                                  : Vectmdcu
**              Priority                                   : 1
**              ISR name                                   : isrVectmdcu
**          Initialization                                 : 
**            Call Init method                             : yes
**            Timer                                        : Disabled
**            Modulus Counter                              : Disabled
**            Pulse Accumulator A                          : Disabled
**            Pulse Accumulator B                          : Disabled
**            Pulse accumulator 0                          : Disabled
**            Pulse accumulator 1                          : Disabled
**            Pulse accumulator 2                          : Disabled
**            Pulse accumulator 3                          : Disabled
**     Contents  :
**         Init - void ECT1_Init(void);
**
**     Copyright : 1997 - 2011 Freescale Semiconductor, Inc. All Rights Reserved.
**     
**     http      : www.freescale.com
**     mail      : support@freescale.com
** ###################################################################*/

#ifndef __ECT1
#define __ECT1

/* MODULE ECT1. */

/*Include shared modules, which are used for whole project*/
#include "PE_Types.h"
#include "PE_Error.h"
#include "PE_Const.h"
#include "IO_Map.h"
/* Include inherited components */

#include "Cpu.h"




void ECT1_Init(void);
/*
** ===================================================================
**     Method      :  ECT1_Init (component Init_ECT)
**
**     Description :
**         This method initializes registers of the ECT module
**         according to this Peripheral Initialization Bean settings.
**         Call this method in user code to initialize the module. By
**         default, the method is called by PE automatically; see "Call
**         Init method" property of the bean for more details.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/

/*
** ===================================================================
** The interrupt service routine must be implemented by user in one
** of the user modules (see ECT1.c file for more information).
** ===================================================================
*/
#pragma CODE_SEG __NEAR_SEG NON_BANKED
__interrupt void isrVectch0(void);
__interrupt void isrVectch1(void);
__interrupt void isrVectch2(void);
__interrupt void isrVectch3(void);
__interrupt void isrVectch4(void);
__interrupt void isrVectch5(void);
__interrupt void isrVectch6(void);
__interrupt void isrVectch7(void);
__interrupt void timerInterrupt(void);
__interrupt void isrVectpaaovf(void);
__interrupt void isrVectpaie(void);
__interrupt void isrVectpabovf(void);
__interrupt void isrVectmdcu(void);
#pragma CODE_SEG DEFAULT
/* END ECT1. */

#endif /* ifndef __ECT1 */
/*
** ###################################################################
**
**     This file was created by Processor Expert 3.05 [04.46]
**     for the Freescale HCS12X series of microcontrollers.
**
** ###################################################################
*/
