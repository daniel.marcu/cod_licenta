//#ifndef _STDINTH_
//#define   _STDINTH_    1

#ifndef int8_t
typedef signed char int8_t;
#endif

#ifndef int16_t
typedef signed int   int16_t;
#endif

#ifndef int32_t
typedef signed long int    int32_t;
#endif

#ifndef int64_t
typedef signed long long int64_t;
#endif

#ifndef uint8_t
typedef unsigned char       uint8_t;
#endif

#ifndef uint16_t
typedef unsigned int  uint16_t;
#endif

#ifndef uint32_t
typedef unsigned long int   uint32_t;
#endif

#ifndef uint64_t
typedef unsigned long long uint64_t;
#endif

#ifndef size_t
typedef unsigned int size_t;
#endif


#ifndef uint64_def
#define uint64_def 1
typedef struct{
   uint8_t data[8];
}uint64;
#endif

//#endif
