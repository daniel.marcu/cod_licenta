/*
This code was taken from the SPHINCS reference implementation and is public domain.
*/


#include "utils.h"

#include "randombytes.h"

static int32_t fd = -1;

  
unsigned char rand_byte(){
    unsigned char i;
    return i;
}

uint32_t read_random(unsigned char *x, uint32_t len){
    uint32_t l=0;
    for(l=0;l<len;l++){
        x[l] = rand_byte();
    }
    return l;
}

void randombytes(unsigned char *x, uint32_t xlen)
{
    uint32_t i;

    if (fd == -1) {

    }

    while (xlen > 0) {
        if (xlen < 1048576) {
            i = xlen;
        }
        else {
            i = 1048576;
        }

        i = read_random(x, i);
        if (i < 1) {
            
            continue;
        }

        x += i;
        xlen -= i;
    }
}