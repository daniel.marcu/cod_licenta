################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Libraries/kyber-main/cbd.c \
../Libraries/kyber-main/fips202.c \
../Libraries/kyber-main/indcpa.c \
../Libraries/kyber-main/kem.c \
../Libraries/kyber-main/ntt.c \
../Libraries/kyber-main/poly.c \
../Libraries/kyber-main/polyvec.c \
../Libraries/kyber-main/reduce.c \
../Libraries/kyber-main/symmetric-shake.c \
../Libraries/kyber-main/verify.c 

COMPILED_SRCS += \
./Libraries/kyber-main/cbd.src \
./Libraries/kyber-main/fips202.src \
./Libraries/kyber-main/indcpa.src \
./Libraries/kyber-main/kem.src \
./Libraries/kyber-main/ntt.src \
./Libraries/kyber-main/poly.src \
./Libraries/kyber-main/polyvec.src \
./Libraries/kyber-main/reduce.src \
./Libraries/kyber-main/symmetric-shake.src \
./Libraries/kyber-main/verify.src 

C_DEPS += \
./Libraries/kyber-main/cbd.d \
./Libraries/kyber-main/fips202.d \
./Libraries/kyber-main/indcpa.d \
./Libraries/kyber-main/kem.d \
./Libraries/kyber-main/ntt.d \
./Libraries/kyber-main/poly.d \
./Libraries/kyber-main/polyvec.d \
./Libraries/kyber-main/reduce.d \
./Libraries/kyber-main/symmetric-shake.d \
./Libraries/kyber-main/verify.d 

OBJS += \
./Libraries/kyber-main/cbd.o \
./Libraries/kyber-main/fips202.o \
./Libraries/kyber-main/indcpa.o \
./Libraries/kyber-main/kem.o \
./Libraries/kyber-main/ntt.o \
./Libraries/kyber-main/poly.o \
./Libraries/kyber-main/polyvec.o \
./Libraries/kyber-main/reduce.o \
./Libraries/kyber-main/symmetric-shake.o \
./Libraries/kyber-main/verify.o 


# Each subdirectory must supply rules for building sources it contributes
Libraries/kyber-main/%.src: ../Libraries/kyber-main/%.c Libraries/kyber-main/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: TASKING C/C++ Compiler'
	cctc -cs --dep-file="$(basename $@).d" --misrac-version=2004 -D__CPU__=tc27xd "-fC:/Users/marcu/AURIX-v1.9.20-workspace/Blinky_LED_1_KIT_TC275_SB/Debug/TASKING_C_C___Compiler-Include_paths__-I_.opt" --iso=99 --c++14 --uchar --language=+volatile --exceptions --anachronisms --fp-model=0 -O3 --tradeoff=4 --compact-max-size=200 -Wc-g3 -Wc-w544 -Wc-w557 -Ctc27xd -Y0 -N0 -Z0 -o "$@" "$<" && \
	if [ -f "$(basename $@).d" ]; then sed.exe -r  -e 's/\b(.+\.o)\b/Libraries\/kyber-main\/\1/g' -e 's/\\/\//g' -e 's/\/\//\//g' -e 's/"//g' -e 's/([a-zA-Z]:\/)/\L\1/g' -e 's/\d32:/@TARGET_DELIMITER@/g; s/\\\d32/@ESCAPED_SPACE@/g; s/\d32/\\\d32/g; s/@ESCAPED_SPACE@/\\\d32/g; s/@TARGET_DELIMITER@/\d32:/g' "$(basename $@).d" > "$(basename $@).d_sed" && cp "$(basename $@).d_sed" "$(basename $@).d" && rm -f "$(basename $@).d_sed" 2>/dev/null; else echo 'No dependency file to process';fi
	@echo 'Finished building: $<'
	@echo ' '

Libraries/kyber-main/%.o: ./Libraries/kyber-main/%.src Libraries/kyber-main/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: TASKING Assembler'
	astc -Og -Os --no-warnings= --error-limit=42 -o  "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-Libraries-2f-kyber-2d-main

clean-Libraries-2f-kyber-2d-main:
	-$(RM) ./Libraries/kyber-main/cbd.d ./Libraries/kyber-main/cbd.o ./Libraries/kyber-main/cbd.src ./Libraries/kyber-main/fips202.d ./Libraries/kyber-main/fips202.o ./Libraries/kyber-main/fips202.src ./Libraries/kyber-main/indcpa.d ./Libraries/kyber-main/indcpa.o ./Libraries/kyber-main/indcpa.src ./Libraries/kyber-main/kem.d ./Libraries/kyber-main/kem.o ./Libraries/kyber-main/kem.src ./Libraries/kyber-main/ntt.d ./Libraries/kyber-main/ntt.o ./Libraries/kyber-main/ntt.src ./Libraries/kyber-main/poly.d ./Libraries/kyber-main/poly.o ./Libraries/kyber-main/poly.src ./Libraries/kyber-main/polyvec.d ./Libraries/kyber-main/polyvec.o ./Libraries/kyber-main/polyvec.src ./Libraries/kyber-main/reduce.d ./Libraries/kyber-main/reduce.o ./Libraries/kyber-main/reduce.src ./Libraries/kyber-main/symmetric-shake.d ./Libraries/kyber-main/symmetric-shake.o ./Libraries/kyber-main/symmetric-shake.src ./Libraries/kyber-main/verify.d ./Libraries/kyber-main/verify.o ./Libraries/kyber-main/verify.src

.PHONY: clean-Libraries-2f-kyber-2d-main

