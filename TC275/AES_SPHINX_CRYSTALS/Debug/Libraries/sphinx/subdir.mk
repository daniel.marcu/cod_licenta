################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Libraries/sphinx/address.c \
../Libraries/sphinx/fips202.c \
../Libraries/sphinx/fors.c \
../Libraries/sphinx/hash_shake.c \
../Libraries/sphinx/main.c \
../Libraries/sphinx/merkle.c \
../Libraries/sphinx/randombytes.c \
../Libraries/sphinx/sign.c \
../Libraries/sphinx/thash_shake_simple.c \
../Libraries/sphinx/utils.c \
../Libraries/sphinx/utilsx1.c \
../Libraries/sphinx/wots.c \
../Libraries/sphinx/wotsx1.c 

COMPILED_SRCS += \
./Libraries/sphinx/address.src \
./Libraries/sphinx/fips202.src \
./Libraries/sphinx/fors.src \
./Libraries/sphinx/hash_shake.src \
./Libraries/sphinx/main.src \
./Libraries/sphinx/merkle.src \
./Libraries/sphinx/randombytes.src \
./Libraries/sphinx/sign.src \
./Libraries/sphinx/thash_shake_simple.src \
./Libraries/sphinx/utils.src \
./Libraries/sphinx/utilsx1.src \
./Libraries/sphinx/wots.src \
./Libraries/sphinx/wotsx1.src 

C_DEPS += \
./Libraries/sphinx/address.d \
./Libraries/sphinx/fips202.d \
./Libraries/sphinx/fors.d \
./Libraries/sphinx/hash_shake.d \
./Libraries/sphinx/main.d \
./Libraries/sphinx/merkle.d \
./Libraries/sphinx/randombytes.d \
./Libraries/sphinx/sign.d \
./Libraries/sphinx/thash_shake_simple.d \
./Libraries/sphinx/utils.d \
./Libraries/sphinx/utilsx1.d \
./Libraries/sphinx/wots.d \
./Libraries/sphinx/wotsx1.d 

OBJS += \
./Libraries/sphinx/address.o \
./Libraries/sphinx/fips202.o \
./Libraries/sphinx/fors.o \
./Libraries/sphinx/hash_shake.o \
./Libraries/sphinx/main.o \
./Libraries/sphinx/merkle.o \
./Libraries/sphinx/randombytes.o \
./Libraries/sphinx/sign.o \
./Libraries/sphinx/thash_shake_simple.o \
./Libraries/sphinx/utils.o \
./Libraries/sphinx/utilsx1.o \
./Libraries/sphinx/wots.o \
./Libraries/sphinx/wotsx1.o 


# Each subdirectory must supply rules for building sources it contributes
Libraries/sphinx/%.src: ../Libraries/sphinx/%.c Libraries/sphinx/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: TASKING C/C++ Compiler'
	cctc -cs --dep-file="$(basename $@).d" --misrac-version=2004 -D__CPU__=tc27xd "-fC:/Users/marcu/AURIX-v1.9.20-workspace/Blinky_LED_1_KIT_TC275_SB/Debug/TASKING_C_C___Compiler-Include_paths__-I_.opt" --iso=99 --c++14 --uchar --language=+volatile --exceptions --anachronisms --fp-model=0 -O3 --tradeoff=4 --compact-max-size=200 -Wc-g3 -Wc-w544 -Wc-w557 -Ctc27xd -Y0 -N0 -Z0 -o "$@" "$<" && \
	if [ -f "$(basename $@).d" ]; then sed.exe -r  -e 's/\b(.+\.o)\b/Libraries\/sphinx\/\1/g' -e 's/\\/\//g' -e 's/\/\//\//g' -e 's/"//g' -e 's/([a-zA-Z]:\/)/\L\1/g' -e 's/\d32:/@TARGET_DELIMITER@/g; s/\\\d32/@ESCAPED_SPACE@/g; s/\d32/\\\d32/g; s/@ESCAPED_SPACE@/\\\d32/g; s/@TARGET_DELIMITER@/\d32:/g' "$(basename $@).d" > "$(basename $@).d_sed" && cp "$(basename $@).d_sed" "$(basename $@).d" && rm -f "$(basename $@).d_sed" 2>/dev/null; else echo 'No dependency file to process';fi
	@echo 'Finished building: $<'
	@echo ' '

Libraries/sphinx/%.o: ./Libraries/sphinx/%.src Libraries/sphinx/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: TASKING Assembler'
	astc -Og -Os --no-warnings= --error-limit=42 -o  "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-Libraries-2f-sphinx

clean-Libraries-2f-sphinx:
	-$(RM) ./Libraries/sphinx/address.d ./Libraries/sphinx/address.o ./Libraries/sphinx/address.src ./Libraries/sphinx/fips202.d ./Libraries/sphinx/fips202.o ./Libraries/sphinx/fips202.src ./Libraries/sphinx/fors.d ./Libraries/sphinx/fors.o ./Libraries/sphinx/fors.src ./Libraries/sphinx/hash_shake.d ./Libraries/sphinx/hash_shake.o ./Libraries/sphinx/hash_shake.src ./Libraries/sphinx/main.d ./Libraries/sphinx/main.o ./Libraries/sphinx/main.src ./Libraries/sphinx/merkle.d ./Libraries/sphinx/merkle.o ./Libraries/sphinx/merkle.src ./Libraries/sphinx/randombytes.d ./Libraries/sphinx/randombytes.o ./Libraries/sphinx/randombytes.src ./Libraries/sphinx/sign.d ./Libraries/sphinx/sign.o ./Libraries/sphinx/sign.src ./Libraries/sphinx/thash_shake_simple.d ./Libraries/sphinx/thash_shake_simple.o ./Libraries/sphinx/thash_shake_simple.src ./Libraries/sphinx/utils.d ./Libraries/sphinx/utils.o ./Libraries/sphinx/utils.src ./Libraries/sphinx/utilsx1.d ./Libraries/sphinx/utilsx1.o ./Libraries/sphinx/utilsx1.src ./Libraries/sphinx/wots.d ./Libraries/sphinx/wots.o ./Libraries/sphinx/wots.src ./Libraries/sphinx/wotsx1.d ./Libraries/sphinx/wotsx1.o ./Libraries/sphinx/wotsx1.src

.PHONY: clean-Libraries-2f-sphinx

