################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Libraries/kyber-dithilium/fips202.c \
../Libraries/kyber-dithilium/ntt.c \
../Libraries/kyber-dithilium/packing.c \
../Libraries/kyber-dithilium/poly.c \
../Libraries/kyber-dithilium/polyvec.c \
../Libraries/kyber-dithilium/reduce.c \
../Libraries/kyber-dithilium/rounding.c \
../Libraries/kyber-dithilium/sign.c \
../Libraries/kyber-dithilium/symmetric-shake.c 

COMPILED_SRCS += \
./Libraries/kyber-dithilium/fips202.src \
./Libraries/kyber-dithilium/ntt.src \
./Libraries/kyber-dithilium/packing.src \
./Libraries/kyber-dithilium/poly.src \
./Libraries/kyber-dithilium/polyvec.src \
./Libraries/kyber-dithilium/reduce.src \
./Libraries/kyber-dithilium/rounding.src \
./Libraries/kyber-dithilium/sign.src \
./Libraries/kyber-dithilium/symmetric-shake.src 

C_DEPS += \
./Libraries/kyber-dithilium/fips202.d \
./Libraries/kyber-dithilium/ntt.d \
./Libraries/kyber-dithilium/packing.d \
./Libraries/kyber-dithilium/poly.d \
./Libraries/kyber-dithilium/polyvec.d \
./Libraries/kyber-dithilium/reduce.d \
./Libraries/kyber-dithilium/rounding.d \
./Libraries/kyber-dithilium/sign.d \
./Libraries/kyber-dithilium/symmetric-shake.d 

OBJS += \
./Libraries/kyber-dithilium/fips202.o \
./Libraries/kyber-dithilium/ntt.o \
./Libraries/kyber-dithilium/packing.o \
./Libraries/kyber-dithilium/poly.o \
./Libraries/kyber-dithilium/polyvec.o \
./Libraries/kyber-dithilium/reduce.o \
./Libraries/kyber-dithilium/rounding.o \
./Libraries/kyber-dithilium/sign.o \
./Libraries/kyber-dithilium/symmetric-shake.o 


# Each subdirectory must supply rules for building sources it contributes
Libraries/kyber-dithilium/%.src: ../Libraries/kyber-dithilium/%.c Libraries/kyber-dithilium/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: TASKING C/C++ Compiler'
	cctc -cs --dep-file="$(basename $@).d" --misrac-version=2004 -D__CPU__=tc27xd "-fC:/Users/marcu/AURIX-v1.9.20-workspace/Blinky_LED_1_KIT_TC275_SB/Debug/TASKING_C_C___Compiler-Include_paths__-I_.opt" --iso=99 --c++14 --uchar --language=+volatile --exceptions --anachronisms --fp-model=0 -O3 --tradeoff=4 --compact-max-size=200 -Wc-g3 -Wc-w544 -Wc-w557 -Ctc27xd -Y0 -N0 -Z0 -o "$@" "$<" && \
	if [ -f "$(basename $@).d" ]; then sed.exe -r  -e 's/\b(.+\.o)\b/Libraries\/kyber-dithilium\/\1/g' -e 's/\\/\//g' -e 's/\/\//\//g' -e 's/"//g' -e 's/([a-zA-Z]:\/)/\L\1/g' -e 's/\d32:/@TARGET_DELIMITER@/g; s/\\\d32/@ESCAPED_SPACE@/g; s/\d32/\\\d32/g; s/@ESCAPED_SPACE@/\\\d32/g; s/@TARGET_DELIMITER@/\d32:/g' "$(basename $@).d" > "$(basename $@).d_sed" && cp "$(basename $@).d_sed" "$(basename $@).d" && rm -f "$(basename $@).d_sed" 2>/dev/null; else echo 'No dependency file to process';fi
	@echo 'Finished building: $<'
	@echo ' '

Libraries/kyber-dithilium/%.o: ./Libraries/kyber-dithilium/%.src Libraries/kyber-dithilium/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: TASKING Assembler'
	astc -Og -Os --no-warnings= --error-limit=42 -o  "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-Libraries-2f-kyber-2d-dithilium

clean-Libraries-2f-kyber-2d-dithilium:
	-$(RM) ./Libraries/kyber-dithilium/fips202.d ./Libraries/kyber-dithilium/fips202.o ./Libraries/kyber-dithilium/fips202.src ./Libraries/kyber-dithilium/ntt.d ./Libraries/kyber-dithilium/ntt.o ./Libraries/kyber-dithilium/ntt.src ./Libraries/kyber-dithilium/packing.d ./Libraries/kyber-dithilium/packing.o ./Libraries/kyber-dithilium/packing.src ./Libraries/kyber-dithilium/poly.d ./Libraries/kyber-dithilium/poly.o ./Libraries/kyber-dithilium/poly.src ./Libraries/kyber-dithilium/polyvec.d ./Libraries/kyber-dithilium/polyvec.o ./Libraries/kyber-dithilium/polyvec.src ./Libraries/kyber-dithilium/reduce.d ./Libraries/kyber-dithilium/reduce.o ./Libraries/kyber-dithilium/reduce.src ./Libraries/kyber-dithilium/rounding.d ./Libraries/kyber-dithilium/rounding.o ./Libraries/kyber-dithilium/rounding.src ./Libraries/kyber-dithilium/sign.d ./Libraries/kyber-dithilium/sign.o ./Libraries/kyber-dithilium/sign.src ./Libraries/kyber-dithilium/symmetric-shake.d ./Libraries/kyber-dithilium/symmetric-shake.o ./Libraries/kyber-dithilium/symmetric-shake.src

.PHONY: clean-Libraries-2f-kyber-2d-dithilium

