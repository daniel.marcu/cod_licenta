#ifndef SPX_RANDOMBYTES_H
#define SPX_RANDOMBYTES_H

extern unsigned char rand_byte();
extern void randombytes(unsigned char * x,unsigned long long xlen);
extern uint32_t read_random(unsigned char *x, uint32_t len);


#endif
