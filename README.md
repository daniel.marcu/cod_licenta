# Evaluarea algoritmilor criptografici post-cuantum pe microcontrolere automotive
Link repository: https://gitlab.upt.ro/daniel.marcu/cod_licenta

# Pașii de rulare ai algoritmilor pe microcontrolerul S12XE
1. Instalarea Freescale CodeWarrior. Acesta poate fi descărcat și instalat de la adresa următoare: https://www.nxp.com/design/design-center/software/development-software/codewarrior-development-tools/downloads:CW_DOWNLOADS
2. Deschiderea proiectului: Pentru a deschide proiectul, din meniul File > Open Project al aplicației CodeWarrior, se va deschide fișierul cu extensia .mcp al fiecărui proiect
3. Pentru a rula programul, codul trebuie compilat din meniul Project > Make, apoi rulat cu debuggerul, de la meniul Project > Debug

# Pasii de rulare ai algoritmilor pe microcontrolerul TC275
1. Instalarea Aurix Development Studio. Acesta poate fi descărcat și instalat de la adresa următoare: https://www.infineon.com/cms/en/product/promopages/aurix-development-studio/
2. Importarea proiectului: De la meniul File > Import Projects From File System se va naviga până la locația unde a fost descărcat proiectul. Se va alege directorul, apoi se va importa
3. Pentru a rula programul, codul trebuie compilat din meniul Project > Build Project, apoi rulat cu debuggerul, de la meniul Debug > Debug Active Project